<?php

use Dingo\Api\Routing\Router;

/** @var Router $api */
$api = app(Router::class);

$api->version('v1', function (Router $api) {
    $api->group(['middleware' => 'cors', 'prefix' => 'auth'], function(Router $api) {
        $api->post('signup', 'App\\Api\\V1\\Controllers\\SignUpController@signUp');
        $api->post('login', 'App\\Api\\V1\\Controllers\\LoginController@login');

        $api->post('recovery', 'App\\Api\\V1\\Controllers\\ForgotPasswordController@sendResetEmail');
        $api->post('reset', 'App\\Api\\V1\\Controllers\\ResetPasswordController@resetPassword');

        $api->post('logout', 'App\\Api\\V1\\Controllers\\LogoutController@logout');
        $api->post('refresh', 'App\\Api\\V1\\Controllers\\RefreshController@refresh');
    });
    
    $api->group(['middleware' => 'cors'], function(Router $api) {
        $api->post('upload', 'App\Api\V1\Controllers\UploadController@upload');   
        $api->get('report/{url}', 'App\Api\V1\Controllers\ReportController@getPDF');
        $api->get('settings', 'App\Api\V1\Controllers\SettingController@index');        
        $api->get('testemail', 'App\Api\V1\Controllers\ReportController@sendTestEmail');
        
    }); 


    $api->group(['middleware' => 'cors', 'middleware' => 'jwt.auth'], function(Router $api) {

        $api->get('refresh', [
            'middleware' => 'jwt.refresh',
            function() {
                return response()->json([
                    'message' => 'By accessing this endpoint, you can refresh your access token at each request. Check out this response headers!'
                ]);
            }
        ]);
        
        $api->get('users/me', 'App\Api\V1\Controllers\UserController@me');
        $api->put('users/me', 'App\Api\V1\Controllers\UserController@updateme'); 
        
        $api->resource('profiles', 'App\Api\V1\Controllers\ProfileController');
        $api->resource('fields', 'App\Api\V1\Controllers\FieldController');
        $api->resource('answers', 'App\Api\V1\Controllers\AnswerController');
        $api->resource('stats', 'App\Api\V1\Controllers\StatsController');       
        $api->get('roles', 'App\Api\V1\Controllers\RoleController@index');       
        $api->get('fieldgroups', 'App\Api\V1\Controllers\FieldController@fieldGroups');        
        $api->post('fields/reorder', 'App\Api\V1\Controllers\FieldController@reorderFields');       
        $api->get('fieldscount', 'App\Api\V1\Controllers\FieldController@count');       
        $api->get('report', 'App\Api\V1\Controllers\ReportController@show');
        
               
        $api->get('reporthtml', 'App\Api\V1\Controllers\ReportController@getReportHtml');
               
        $api->post('reporthtml', 'App\Api\V1\Controllers\ReportController@generatePdfFromHtml');
        
        $api->get('adminreport', 'App\Api\V1\Controllers\ReportController@showadmin');
        $api->get('export/{id}', 'App\Api\V1\Controllers\AdminController@export');
        $api->get('export', 'App\Api\V1\Controllers\AdminController@exportall');  
        $api->get('exportanswers', 'App\Api\V1\Controllers\AdminController@exportanswers');     
        $api->get('exportaverages', 'App\Api\V1\Controllers\AdminController@exportaverages');           
        $api->get('view', 'App\Api\V1\Controllers\AdminController@view');    
        $api->delete('deletesubmission/{id}', 'App\Api\V1\Controllers\AdminController@destroy');     
        $api->resource('users', 'App\Api\V1\Controllers\UserController');
 
        $api->put('settings/{id}', 'App\Api\V1\Controllers\SettingController@update');   
        
        
    });
});
