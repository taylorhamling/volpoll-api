<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html>
<head>

    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no" />
</head>
<body style="font-family: 'Helvetica Neue', sans-serif;">
<style type="text/css">
    .footer ul li:not(:last-child):after{
        content:"‧";
        font-weight:900;
        color:#ABAFC0;
        margin-left:10px;
        margin-right:10px;
    }
    
@media screen and (max-width:480px){
    body{
        margin:0px;
        max-width:100%;
    }
  .desktop-container{
    margin:0px !important;
    border:none !important;
    box-shadow:none !important;
    width:auto !important;
    height:auto !important;
    max-width: 100%;
  }
  .desktop-container h1, .desktop-container p, .desktop-container img, .desktop-container hr{
    max-width: 100% !important;
    text-align: left !important;
    width: 100% !important;
  }
  .desktop-container h1, .desktop-container p, .footer p , .footer ul{
      padding-left:10% !important;
      padding-right:10% !important;
      width: 80% !important;
  }
  
  .footer ul{
      display:inline-block !important;
  }
  
  .footer, .footer p{
      width:auto !important;
  }
  
  .footer a{
      line-height:20px;
  }
}    
</style>

<div class="desktop-container" style="box-sizing: border-box; height: 762px; width: 642px; border-radius: 3px; background-color: #FFFFFF; box-shadow: 0 30px 70px 0 rgba(25,48,72,0.05); text-align: center; margin: 50px auto 0; border: 1px solid #d4d7e1;" align="center">
    <img class="header-image" src="https://i.imgur.com/ORpn5ND.png" style="width: 642px;">
    
    <h1 style="width: 500px; color: #474B61; font-family: 'Helvetica Neue', sans-serif; font-size: 28px; font-weight: bold; line-height: 32px; text-align: center; padding-top: 40px; padding-bottom: 20px; margin: 0 auto;" align="center"><?php echo $profile['first_name']; ?>, your status report is ready!</h1>
    
    <p style="width: 500px; opacity: 0.8; color: #474B61; font-family: 'Helvetica Neue', sans-serif; font-size: 16px; line-height: 24px; text-align: center; margin: 0 auto 40px;" align="center">
        This report is designed to give you a practical evaluation that you can apply immediately, by  helping you reflect and identify opportunities for your organisation’s volunteer management.<br>
It contains a reflection area regarding your assessment, an action plan, and additional resources to help you improve your organisation’s volunteer programme.        
        
        
    </p>
    
    <a class="download-button" href="https://volpoll.org.au/api/public/api/report/<?php echo $report['url']; ?>" style="color: #FFFFFF; text-decoration: none; height: 50px; width: 221px; border-radius: 100px; font-family: 'Helvetica Neue', sans-serif; font-size: 16px; font-weight: bold; line-height: 20px; text-align: center; background-color: #CC2D43; padding: 15px 20px;">Download my report</a>
    
    <hr style="margin-top: 60px; box-sizing: border-box; height: 2px; width: 500px; border: 1px solid #abafc0;">
    
    
    <h1 style="width: 500px; color: #474B61; font-family: 'Helvetica Neue', sans-serif; font-size: 28px; font-weight: bold; line-height: 32px; text-align: center; padding-top: 40px; padding-bottom: 20px; margin: 0 auto;" align="center">More useful resources</h1>
    
    <p style="width: 500px; opacity: 0.8; color: #474B61; font-family: 'Helvetica Neue', sans-serif; font-size: 16px; line-height: 24px; text-align: center; margin: 0 auto 40px;" align="center">
        <a href="https://www.volunteeringvictoria.org.au/" style="color: #CC2D43; text-decoration: none;">Volunteering Victoria’s website</a> provides information on volunteering, managing volunteers and recent research on aspects of volunteering.
      
        
    </p>
    
</div>


<div class="footer" style="font-family: 'Helvetica Neue', sans-serif; height: 40px; width: 504px; color: #ABAFC0; font-size: 13px; font-weight: bold; line-height: 10px; text-align: center; margin: 70px auto 0;" align="center">
    <ul style="display: inline; padding-left: 0px;">
        <li style="display: inline;"><a href="mailto:info@volunteeringvictoria.org.au" style="color: #ABAFC0; text-decoration: none;">Contact</a></li>
        <li style="display: inline;"><a href="https://www.volunteeringvictoria.org.au/about-us/" style="color: #ABAFC0; text-decoration: none;">Learn more</a></li>
        <li style="display: inline;"><a href="https://www.volunteeringvictoria.org.au/disclaimer/" style="color: #ABAFC0; text-decoration: none;">Terms of Use</a></li>
    </ul>
    
    <p style="width: 500px; opacity: 0.8; color: #ABAFC0; font-family: 'Helvetica Neue', sans-serif; font-size: 13px; line-height: 24px; text-align: center; font-weight: 300; margin: 10px auto 20px;" align="center">
        Copyright &copy; <?php echo date("Y"); ?> Volunteering Victoria, All rights reserved.         
    </p>
    

</a>
</div>
</body>
</html>