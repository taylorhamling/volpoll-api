<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" "http://www.w3.org/TR/REC-html40/loose.dtd">
<html>
<head>

    <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no" />
</head>
<body style="font-family: 'Helvetica Neue', sans-serif;">
<style type="text/css">
    .footer ul li:not(:last-child):after{
        content:"‧";
        font-weight:900;
        color:#ABAFC0;
        margin-left:10px;
        margin-right:10px;
    }
    
@media screen and (max-width:480px){
    body{
        margin:0px;
        max-width:100%;
    }
  .desktop-container{
    margin:0px !important;
    border:none !important;
    box-shadow:none !important;
    width:auto !important;
    height:auto !important;
    max-width: 100%;
  }
  .desktop-container h1, .desktop-container p, .desktop-container img, .desktop-container hr{
    max-width: 100% !important;
    text-align: left !important;
    width: 100% !important;
  }
  .desktop-container h1, .desktop-container p, .footer p , .footer ul{
      padding-left:10% !important;
      padding-right:10% !important;
      width: 80% !important;
  }
  
  .footer ul{
      display:inline-block !important;
  }
  
  .footer, .footer p{
      width:auto !important;
  }
  
  .footer a{
      line-height:20px;
  }
}    
</style>

<div class="desktop-container" style="box-sizing: border-box; height: 762px; width: 642px; border-radius: 3px; background-color: #FFFFFF; box-shadow: 0 30px 70px 0 rgba(25,48,72,0.05); text-align: center; margin: 50px auto 0; border: 1px solid #d4d7e1;" align="center">
    <img class="header-image" src="https://i.imgur.com/ORpn5ND.png" style="width: 642px;">
    
    <h1 style="width: 500px; color: #474B61; font-family: 'Helvetica Neue', sans-serif; font-size: 28px; font-weight: bold; line-height: 32px; text-align: center; padding-top: 40px; padding-bottom: 20px; margin: 0 auto;" align="center">Welcome, <?php echo $profile['first_name']; ?>!</h1>
    

    
    <h1 style="width: 500px; color: #474B61; font-family: 'Helvetica Neue', sans-serif; font-size: 28px; font-weight: bold; line-height: 32px; text-align: center; padding-top: 40px; padding-bottom: 20px; margin: 0 auto;" align="center"> To get the most out of your experience, we want to share with you some more resources to assist you with your volunteer leadership:</h1>
    
    <p style="width: 500px; opacity: 0.8; color: #474B61; font-family: 'Helvetica Neue', sans-serif; font-size: 16px; line-height: 24px; text-align: center; margin: 0 auto 40px;" align="center">
        
        <a href="https://www.volpoll.org.au/volpoll-plus" style="color: #CC2D43; text-decoration: none;">Introducing VolPoll+</a>: Go beyond individual insights with VolPoll +. Make the most out this unique platform by gaining further insights into your volunteer workforce. Want to learn more? <a href="mailto:info@volunteeringvictoria.org.au">Contact us</a>.<br><br>
        
        <a href="https://www.volpoll.org.au" style="color: #CC2D43; text-decoration: none;">Share VolPoll</a>: Forward VolPoll to someone you think needs it. It’s easy; just send them the following link to get them started: https://volpoll.org.au.<br><br>
        
        <a href="https://volpoll.org.au/wp-content/uploads/2020/05/Volpoll-faqs.pdf" style="color: #CC2D43; text-decoration: none;">FAQS</a>: Have questions about VolPoll? Read our FAQs.<br><br>
        
        <a href="mailto:info@volunteeringvictoria.org.au" style="color: #CC2D43; text-decoration: none;">Contact Us</a>: If you have any questions about VolPoll or VolPoll+, please don’t hesitate to contact us.
        
    </p>
    
</div>


<div class="footer" style="font-family: 'Helvetica Neue', sans-serif; height: 40px; width: 504px; color: #ABAFC0; font-size: 13px; font-weight: bold; line-height: 10px; text-align: center; margin: 70px auto 0;" align="center">
    <ul style="display: inline; padding-left: 0px;">
        <li style="display: inline;"><a href="mailto:info@volunteeringvictoria.org.au" style="color: #ABAFC0; text-decoration: none;">Contact</a></li>
        <li style="display: inline;"><a href="https://www.volunteeringvictoria.org.au/about-us/" style="color: #ABAFC0; text-decoration: none;">Learn more</a></li>
        <li style="display: inline;"><a href="https://www.volunteeringvictoria.org.au/disclaimer/" style="color: #ABAFC0; text-decoration: none;">Terms of Use</a></li>
    </ul>
    
    <p style="width: 500px; opacity: 0.8; color: #ABAFC0; font-family: 'Helvetica Neue', sans-serif; font-size: 13px; line-height: 24px; text-align: center; font-weight: 300; margin: 10px auto 20px;" align="center">
        Copyright &copy; <?php echo date("Y"); ?> Volunteering Victoria, All rights reserved.         
    </p>
    

</a>
</div>
</body>
</html>