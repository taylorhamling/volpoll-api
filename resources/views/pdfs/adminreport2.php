<?php 
    if ($_SERVER['REQUEST_METHOD'] !== "POST" || empty($_GET["url"])){
        //die();
    }

    
error_reporting(0);
@ini_set('display_errors', 0);
    


    date_default_timezone_set('Australia/Melbourne');
    $currentDate = date('F j Y', time());
?>



<html>
    <head>    
        <title>VolPoll Report</title>
        <style>

                
                @page main{
                    size: 8.3in 11.7in;
                    footer: page-footer;
                    margin: 5% 10%;
                }
                               
                
                .footer-container{
                    height: 52px;
                    margin: 30pt 0pt;
                    display: block;
                    border-top: 1px solid #a8b5c3;
                    opacity: 0.4;
                    color: #999999;
                    font-size: 9pt;
                    line-height: 9pt;
                    font-family: "montserrat";
                    font-weight:bold;
                    display:none;
                }
                
                .footer-container .footer-date{
                    text-align:left;
                    padding-top: 20px;
                }
                .footer-container .footer-page{
                    text-align:right;
                    padding-top: 20px;
                }                

                body{
                    margin:0px;
                    font-size:10.5pt;
                    line-height:14pt;
                    font-family:"montserrat", "Open Sans", Sans-Serif;
                    max-width:8.3in;
                    color:#2e476b;
                    
                }

                .page{
                    page: main;
                    page-break-before: always;
                    min-width:100%;
                    max-width:100%;
                    width:100%;
                }
                
                .first-page{
                    page-break-before: avoid;
                    min-width:100%;
                    max-width:100%;
                    width:100%;  
                        margin:0px;
                        padding:0px;
                        position:relative;
                }
                
                .cover-image{
                    min-width:100%;
                    max-width:100%;
                    width:100%;
                        margin:0px;
                        padding:0px;
                }

                .main-header{
                    font-family: "montserrat";
                    font-weight:bold;
                    font-size: 24pt;
                    line-height: 24pt;
                    color:#2e476b;
                    margin-bottom:40px;
                }
                .sub-header
                {
                        font-family: "montserrat";
                        font-weight:bold;
                        font-size:12pt;
                        line-height: 12pt;
                        color: #2e476b;
                        margin-bottom: 20px;
                }  
                
                p{
                    margin-bottom:13px;
                }
                
                
                /*COVER PAGE **************************************************/
                
                .cover-date{
                    
                    font-family: "montserrat";
                    font-weight:bold;
                    font-size: 13pt;
                    line-height: 13pt;
                        color: white;   
                        text-align:right;
                }
                
                .cover-title{
                    font-family: "montserrat";
                    font-weight:bold;
                    font-size: 32pt;
                    line-height: 32pt; 
                        color: white;
                        padding-top:80pt;
                }
                
                .cover-sub{
                    font-family: "montserrat";
                    font-weight:bold;
                    text-transform: none;
                    font-size: 16pt;
                    line-height: 16pt;
                    margin-bottom: 15px;   
                    padding-top:20pt;
                        color: white;
                }                
 
                .cover-text{
                    
                    width:100%;
                    height:351pt;
                    padding-left:40pt;
                    padding-right:40pt;
                    padding-top:30pt;
                    background-color:#db1f33;
                }
                
                /*PAGE 1*******************************************************/
                
                
                .vnz-logo{
                    margin-bottom:40px;
                }
                
                /*PAGE 2*******************************************************/
                
                .dimension-list{
                    
                    
                }
                
                .dimension-list .dimension{
                    background-color: rgb(204, 206, 217);
                    display: table;
                    padding: 5px;
                    margin-bottom: 5px;
                }
                
                .dimension-list .dimension .dimension-name{
                        width: 96pt;
                        display: table-cell;
                }                
                .dimension-list .dimension .dimension-description{
                        width: 273pt;
                        display: table-cell;
                }  
                
                /*PAGE 3 ******************************************************/
                
                .graph-container{
                    background-color: #f4f5fa;
                    width: 350pt;  
                    margin-bottom:25px;
                    
                }
                
                .graph-container > img{
                        margin-top: -30px;
                }
                
                /*PAGE 4 ******************************************************/
                
                .results-list{
                        border-top: 1px solid rgba(33, 67, 104, 0.4);
                }
                
                .results-list .result .result-description .result-anchor{
                    font-weight:400;
                    color:#677992;
                    display:block;
                    padding-bottom:5px;
                }                
                
                
                .results-list .result .result-description{
                    background-color: #F4F5FA;
                    border-bottom: 1px solid rgba(33, 67, 104, 0.4);
                        width: 300pt;
                    color: #2e476b;
                    font-size: 10.5pt;
                    font-weight: bold;
                    padding-left: 10px;
                    padding-right: 10px;
                    padding-top: 10px;
                    padding-bottom: 10px;                    
                }
                .results-list .result .result-slider{
                    background-color: #F4F5FA;
                    width: 270px;
                    padding-left: 30px;
                    padding-right: 10px;
                    border-bottom: 1px solid rgba(33, 67, 104, 0.4);
                }
                
                .slider{
                    padding: 30px 10px;
                        position: relative;
                        padding-top: 30px;
                        padding-bottom: 45px;
                        width: 250px;
                        background-color: #f4f5fa;
                }
                
                .top-label{
                    position: absolute;
                    top: 0px;
                    white-space: nowrap;
                    color: #2e4c6e;
                    font-size: 14px;
                    font-weight: 900;
                    text-transform: capitalize;
                    display:none;
                }
                
                .slider-line{
                    border-bottom: 3px solid #2e4c6e;
                }
                .slider-circle-container{
                    position: relative;
                    margin-right: 30px !important;
                    top: 0;
                    left: 0;                    
                }
                .slider-circle{
                    width: 25px;
                    height: 25px;
                    border-radius: 15.5px;
                    background-color: white;
                    border: #2e4c6e solid 3px;
                    position: absolute;
                    top: -17px;

                }
                
                .bottom-label{
                    margin-top:15px;
                    text-transform:uppercase;
                    color:#6f8aaf;
                    font-size: 8px;
                    font-weight: 900;
                    
                }
                
                .left-label{
                    float: left;
                }
                
                .right-label{
                    float: right;
                }                
                
                /*PAGE 5 ******************************************************/
                
                .strategy-header{
                background-color: #1d3f65;
                padding: 10px 15px 13px 15px;
                color: white;
                letter-spacing: 0.5px;
                font-size: 14pt;
                line-height: 14pt;
                border-radius: 2px;
                margin-bottom: 40px;
                    display: inline-block;
                }
                
                /*PAGE 6*******************************************************/
                
                .lines{
                    border-bottom:1px solid rgba(33, 67, 104, 0.4);
                    height: 18pt;
                }
                
                .action-list .action{
                    height: 34pt;
                }
                
                .action-list .action .take{
                        width: 200pt;
                    border-bottom:1px solid rgba(33, 67, 104, 0.4);
                }
                
                .action-list .action .when{
                        width: 200pt;
                    border-bottom:1px solid rgba(33, 67, 104, 0.4);
                }
                
                .action-list .action .review{
                        width: 200pt;
                    border-bottom:1px solid rgba(33, 67, 104, 0.4);
                    
                }
                
                
                
                
                
        </style>     
        <script src="http://volunteeringvictoria.org.au/volpoll/api/public/reports/report.js"></script> 
    </head>

    <body>

        <htmlpagefooter name="page-footer" >   
            <table width="100%" class="footer-container">
                <tr>
                    <td width="50%" align="left" class="footer-date">{DATE j F Y}</td>
                    <td width="50%" align="right" class="footer-page">Page {PAGENO} of {nbpg}</td>
                </tr>
            </table>            
        </htmlpagefooter>  
    
        <htmlpagefooter name="no-display" style="display:none;">
            <div id="container" style="display:none;"></div>
           

            
        </htmlpagefooter>
    
    
        <htmlpagefooter name="no-display2">
                <?php 
            
                foreach ($answers as $answer){
                    if (!isset($answer["skipped"])){
                        $options = $answer["field"]["options"] ? json_decode($answer["field"]["options"]) : array("Strongly Disagree","Disagree","In Between","Agree","Strongly Agree");
                        ?>
                        <div id="slider<?php echo $answer["id"]; ?>" class="slider">
                            <div class="top-label" style="left:<?php echo ((int)$answer["answer"] - 1) * 25; ?>%;"><?php echo $options[(((int)$answer["answer"]) - 1)]; ?></div>
                            <div class="slider-line"></div>
                            <div class="slider-circle-container">
                                <div class="slider-circle" style="left:<?php echo ((int)$answer["answer"] - 1) * 25; ?>%;"></div>
                            </div>
                            <div class="left-label bottom-label"><?php echo $options[0]; ?></div>
                            <div class="right-label bottom-label"><?php echo $options[count($options) - 1]; ?></div>
                        </div>                    
                        <?php
                    }
                }
                
            ?> 
            
        </htmlpagefooter>
    
    
        <div class="first-page" name='first-page'>
            <div class="cover-container">
                <img class="cover-image" src="http://volpoll.org.au/api/public/reports/cover_top.jpg"/>
                <div class="cover-text">
                    <div class="cover-date"><?php echo $currentDate; ?></div>
                    <div class="cover-title"><span class="report-name"><?php echo $profile["first_name"]; ?> <?php echo $profile["last_name"]; ?></span></div>
                    <div class="cover-sub">Here is your personalised VolPoll report</div>
                </div>
                <img class="cover-image" src="http://volpoll.org.au/api/public/reports/cover_bottom.jpg"/>
            </div>
        </div>

        <div class="page">
            <img class="vnz-logo" src="http://volunteeringvictoria.org.au/volpoll/api/public/reports/vv_color.png"/>
            <div class="main-header">Report for <span class="report-name"><?php echo $profile["first_name"] .  (isset($profile["group"]) ? (', ' . $profile["group"]) : ''); ?></span></div>
            
            <div class="sub-header"><?php echo ucfirst($profile["role"]["name"]); ?></div>
            <p>
                VolPoll is an online self-assessment tool looking at some of the challenges faced by volunteer involving <?php echo $profile["group_type"]; ?>s.
            </p>
            <p>
                For many <?php echo $profile["group_type"]; ?>s, the involvement of volunteers makes the difference between being able to function or not.
            </p>
            <p>
                Volunteering in small to medium <?php echo $profile["group_type"]; ?>s is often a long term effort. Volunteers and volunteer leaders are giving back to their communities on top of other responsibilities because they are passionate about what they do. It's important everyone involved has the tools they need to ensure the <?php echo $profile["group_type"]; ?> is working well into the future. 
            </p>
            <br><br>
            <div class="sub-header">What's in your report?</div>
            <p>This report is designed to provide a detailed summary of your responses to all the questions that you have just answered. It covers <?php echo (((count($fieldGroups) - 1) === 3) ? "three" :  (count($fieldGroups) - 1)); ?> main categories:</p>
            <table class="dimension-list">
                
                <?php 
                
                    foreach ($fieldGroups as $fieldGroup){
                        if ($fieldGroup["name"] !== "Tutorial"){
                            
                            ?>

                            <tr class="dimension">
                                <td class="dimension-name"><?php echo str_replace("group", $profile["group_type"], $fieldGroup["name"]); ?></td>
                                <td class="dimension-description"><?php echo (isset($fieldGroup["description"]) ? str_replace("group", $profile["group_type"], $fieldGroup["description"])  : ''); ?></td>
                            </tr>
                            
                            <?php
                        }
                    }
                    
                    ?>
               
            </table>       
            
            
            <p>The report reflects the answers you provided to us and gives you an overview of best practice in each domain. </p>
            <p>The suggestions below are tailored to help you consider how you may be able to improve you practice across each of these domains. </p>
            <p>If you have concerns about how you have scored particularly in the domains of safety consider talking to other leaders at your <?php echo $profile["group_type"]; ?>, talking to your insurance provider, or referring to the <a href="https://www.nfplaw.org.au/sites/default/files/media/NFP_Law_-_National_Volunteer_Guide.pdf">National Volunteer Guide</a> produced by Volunteering Australia and Justice Connect.</p>


            
            

        </div>      
        
        
        <div class="page">

            

            <div class="sub-header">How to use this report</div>
            
            <p>
                This report can be used in a range of ways including: 
            </p>  
            
            <ul class="ul">
                <li>As a structure to think about how your <?php echo $profile["group_type"]; ?> supports its volunteers</li>
                <li>To start a conversation within your <?php echo $profile["group_type"]; ?> on how to support and/ or improve the volunteer experience</li>
                <li>To provide information for planning</li>
                <li>For a conversation about your <?php echo $profile["group_type"]; ?>s volunteer involvement with your management team or your Board</li>
                <li>To improve processes by identifying any gaps and generating ideas for addressing these</li>
                <li>To use when reviewing or refreshing your <?php echo $profile["group_type"]; ?>s strategy for the future.</li>
            </ul>
			
			<p>The results included are designed to give you an overview of your answers in relation to volunteer sustainability, safety and satisfaction and some actions to consider that will strengthen your <?php echo $profile["group_type"]; ?>. We have included space for you to write down the next steps you will take in in relation to volunteer sustainability, safety and satisfaction.</a>
            
            <br><br>
            
            <div class="main-header">Results overview</div>
            
            <p>The chart below provides an overview of how you are performing in each of the three categories – Sustainability, Safety and Satisfaction. The further towards the edge of the spoke you reach for each category the better you are performing in that category.</p>
            
            <br><br>
            <div class='graph-container'>          
                <div id="svg"></div>

                <img src="https://volpoll.org.au/api/public/reports/volpoll_report.png"/>
            </div>
            
        </div>         
        
    
    
    
    
    
    
    
    
    
    
        <?php 
        
        foreach ($fieldGroups as $fieldGroup){
            if ($fieldGroup["name"] !== "Tutorial"){

                ?>        
        
        <div class="page">
            <div class="main-header"><?php echo $fieldGroup["name"]; ?></div>
            
            <p>
                <?php 
                    
                    if (isset($fieldGroup["report_description"])){
                        $reportDescription = json_decode($fieldGroup["report_description"], true);

                        echo str_replace("group", $profile["group_type"], $reportDescription[$profile["role"]["name"]]);
                    }
                ?>
            </p>  
            
            <br>
            <div class="sub-header">Results</div>

            
            
            <table class="results-list">
                
                
                
                <?php 
                    foreach ($answers as $answer){
                        if ($answer["field"]["field_group_id"] === $fieldGroup["id"] 
                                && ($answer["field"]["type"] === "Select" || $answer["field"]["type"] === "Select + Free Text")
                                && $answer["field"]["field_group_id"] !== 2){
                        ?>
                            <tr class="result">
                                <td class="result-description">
                                    <div class="result-anchor" style="color:#677992;font-weight:normal;"><?php echo $answer["field"]["anchor"]; ?></div>
                                    <?php echo str_replace("group", $profile["group_type"], $answer["field"]["question"]); ?>
                                </td>
                                <td class="result-slider" id="slider-image<?php echo $answer["id"]; ?>">
                                    <?php if (!isset($answer["skipped"])){ ?>
                                        <script>
                                            html2canvas(document.getElementById("slider<?php echo $answer["id"]; ?>"), {
                                                onrendered: function(canvas) {
                                                    var img = canvas.toDataURL("image/jpeg");
                                                    var e = document.createElement('img');
                                                    e.src = img;
                                                  document.getElementById("slider-image<?php echo $answer["id"]; ?>").appendChild(e);
                                                  document.getElementById("slider<?php echo $answer["id"]; ?>").style.display = "none";
                                                }
                                            });
                                        

                                        </script>
                                    <?php
                                    }
                                    ?>
                                </td>
                            </tr>
                        <?php
                        }
                    }
                ?>            
            </table>
            
            <br>
        
            
        </div>
    
    
    
        <div class="page">

            <div class="sub-header">Consider</div>

            
            <ul class="ul">
                <?php 
                    if (isset($fieldGroup["report_ideas"])){
                        $reportIdeas = json_decode($fieldGroup["report_ideas"], true);
                        foreach ($reportIdeas[$profile["role"]["name"]] as $item){
                            ?>
                            <li>
                                <?php echo str_replace("group", $profile["group_type"], $item); ?>
                            </li>
                        <?php
                        }
                    }
                    ?>
            </ul>

            
            <br>
            <div class="sub-header">Next Steps:</div>
            <div class="ol">
                <div>1. &nbsp;</div>
                <div>2. &nbsp;</div>
                <div>3. &nbsp;</div>
            </div>  

        </div>
        
    
    
    
    
    

        <?php 
            }
        }
        ?>
    

    
    
        <div class="page">
            <div class="main-header">Resources</div>
            
            
            <?php 
            
                foreach ($resourceLinks as $resource){
                    ?>
                    <p>
                        <strong><?php echo $resource["title"]; ?></strong><br>
                        <a href="<?php echo $resource["link"]; ?>"><?php echo $resource["link"]; ?></a><br>
                        <?php echo (isset($resource["description"]) ? $resource["description"] : ''); ?>
                    </p>
                    <?php
                }
                
            ?>
            
            
        </div>

                <script>


                var chart = Highcharts.chart('container', {

                    chart: {
                        polar: true,
                        type: 'line',
                        backgroundColor: '#f4f5fa'
                    },

                    title: {
                        text: '',
                        x: -80
                    },

                    pane: {
                        size: '80%'
                    },

                    xAxis: {
                        categories: [<?php                        
                            foreach ($fieldGroups as $fieldGroup){
                                if ($fieldGroup["name"] !== "Tutorial"){
                                    echo "'" . $fieldGroup["name"] . "',";
                                }
                            }
                        ?>],
                        tickmarkPlacement: 'on',
                                    tickInterval:1,
                        lineWidth: 0,
                        min: 0,
                        max:<?php echo (count($fieldGroups) - 1); ?>,
                        angle:45,
                        lineColor: '#000'

                    },

                    yAxis: [
                        {
                        gridLineInterpolation: 'polygon',
                        showLastLabel: true,
                        lineWidth: 0,
                        tickInterval:1,
                        min: 0,
                        pane:0,
                        max:5,
                        angle:-45
                        },
                        {
                        showLastLabel: true,
                        gridLineWidth: false,                        
                        lineWidth: 0,
                        tickInterval:1,
                        min: 0,
                        pane:1,
                        max:5,
                        verticalAlign: 'bottom',
                        angle:0,
                        lineColor: '#000'
                        }                     
                    ],
                    pane:[{startAngle:45},{startAngle:90}],
                    tooltip: {
                        shared: true,
                        pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.0f}</b><br/>'
                    },

                    legend: {
                        align: 'right',
                        verticalAlign: 'top',
                        y: 70,
                        layout: 'vertical',
                        enabled:false,
                        itemStyle:{'color':'#000'}
                    },

                    series: [{
                        name: 'Your result',
                        data: [
                            <?php                        
                                foreach ($fieldGroups as $fieldGroup){
                                    if ($fieldGroup["name"] !== "Tutorial"){
                                        echo ($graph[$fieldGroup["id"]] > 0 ? (float) ($graph[$fieldGroup["id"]] / $graphCounts[$fieldGroup["id"]]) : 0) . ",";
                                    }
                                }
                            ?>           
                            ],
                        pointPlacement: 'on',
                        color:"#cc2d43"
                        },
                        {
                            yAxis: 1,
                            data: [],
                            visible: false,
                            color:"#cc2d43"
                        }                    
                    ],
                    credits: {
                        enabled: false
                    }                

                });
                $("#svg").html(chart.getSVG());
                
                //$(".slider").hide();
				
		$("htmlpagefooter[name='no-display']").remove();
                //$("htmlpagefooter[name='no-display2']").remove();


                </script>


    </body>
</html>