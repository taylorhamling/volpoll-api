<?php 
    if ($_SERVER['REQUEST_METHOD'] !== "POST" || empty($_GET["url"])){
        //die();
    }
    
    error_reporting(0);
    @ini_set('display_errors',0);
    
    $averages = json_decode($_POST["averages"],true);
    $polarAverages = json_decode($_POST["polarAverages"],true);
    $textResults = json_decode($_POST["textResults"],true);

   
    date_default_timezone_set('Australia/Melbourne');
    $currentDate = date('F j Y', time());    


?>



<html>
    <head>    
        <title>VolPoll+ Report</title>
        <style>

                
                @page main{
                    size: 8.3in 11.7in;
                    footer: page-footer;
                    margin: 5% 10%;
                    
                    
                }
                               
                
                .footer-container{
                    height: 52px;
                    margin: 30pt 0pt;
                    display: block;
                    border-top: 1px solid #a8b5c3;
                    opacity: 0.4;
                    color: #999999;
                    font-size: 9pt;
                    line-height: 9pt;
                    font-family: "montserrat";
                    display:none;
                }
                
                .footer-container .footer-date{
                    text-align:left;
                    padding-top: 20px;
                }
                .footer-container .footer-page{
                    text-align:right;
                    padding-top: 20px;
                }                

                body{
                    margin:0px;
                    
                    font-size:10.5pt;
                    line-height:14pt;
                    font-family:"open_sans", "Open Sans", Sans-Serif;
                    max-width:8.3in;
                    color:#5A5D71;
                    
                }

                .page{
                    page: main;
                    page-break-before: always;
                    min-width:100%;
                    max-width:100%;
                    width:100%;
                }
                
                .first-page{
                    page-break-before: avoid;
                    min-width:100%;
                    max-width:100%;
                    width:100%;  
                        margin:0px;
                        padding:0px;
                        position:relative;
                }
                
                .cover-image{
                    min-width:100%;
                    max-width:100%;
                    width:100%;
                        margin:0px;
                        padding:0px;
                }

                .main-header{
                    font-family: "montserrat";
                    font-size: 26pt;
                    line-height: 26pt;
                    color:#474B61;
                    margin-bottom:40px;
                }
                .sub-header
                {
                        font-family: "montserrat";
                        font-size:13pt;
                        line-height: 13pt;
                        color: #474B61;
                        margin-bottom: 25px;
                }  
                
                p{
                    margin-bottom:13px;
                }
                
                
                /*COVER PAGE **************************************************/
                
                .cover-date{
                    
                    font-family: "montserrat";
                    font-weight:bold;
                    font-size: 13pt;
                    line-height: 13pt;
                        color: white;   
                        text-align:right;
                }
                
                .cover-title{
                    font-family: "montserrat";
                    font-weight:bold;
                    font-size: 32pt;
                    line-height: 32pt; 
                        color: white;
                        padding-top:80pt;
                }
                
                .cover-sub{
                    font-family: "montserrat";
                    font-weight:bold;
                    text-transform: none;
                    font-size: 16pt;
                    line-height: 16pt;
                    margin-bottom: 15px;   
                    padding-top:20pt;
                        color: white;
                }                
 
                .cover-text{
                    
                    width:100%;
                    height:351pt;
                    padding-left:40pt;
                    padding-right:40pt;
                    padding-top:30pt;
                    background-color:#db1f33;
                    font-family: "montserrat";
                }
                
                /*PAGE 1*******************************************************/
                
                
                .vnz-logo{
                    margin-bottom:40px;
                }
                
                /*PAGE 2*******************************************************/
                
                .dimension-list{
                    
                    
                }
                
                .dimension-list .dimension{
                    background-color: rgb(204, 206, 217);
                    display: table;
                    padding: 5px;
                    margin-bottom: 5px;
                }
                
                .dimension-list .dimension .dimension-name{
                        width: 96pt;
                        display: table-cell;
                }                
                .dimension-list .dimension .dimension-description{
                        width: 273pt;
                        display: table-cell;
                }  
                
                /*PAGE 3 ******************************************************/
                
                .polar-graph-container td div{
                        margin-bottom: 15px;
                }
                
                .graph-container{
                    background-color: #f4f5fa;
                    width: 550pt;  
                    margin-bottom:25px;
                    
                }
                
                .graph-container > div{
                    margin-bottom:0px;
                }
                
                .graph-container > img{
                        margin-top: -50px;
                }
                
                /*PAGE 4 ******************************************************/
                
                .results-list{
                        border-top: 1px solid rgba(33, 67, 104, 0.4);
                }
                
                .results-list .result .result-description{
                    background-color: #F4F5FA;
                    border-bottom: 1px solid rgba(33, 67, 104, 0.4);
                        width: 300pt;
                    color: #5A5D71;
                    font-size: 10.5pt;
                    font-weight: bold;
                    padding-left: 10px;
                    padding-right: 10px;
                    padding-top: 10px;
                    padding-bottom: 10px;                    
                }
                .results-list .result .result-slider{
                    background-color: #F4F5FA;
                    width: 270px;
                    padding-left: 30px;
                    padding-right: 10px;
                    border-bottom: 1px solid rgba(33, 67, 104, 0.4);
                }
                
                .slider{
                    padding: 30px 10px;
                        position: relative;
                        padding-top: 30px;
                        padding-bottom: 45px;
                        width: 250px;
                        background-color: #f4f5fa;
                }
                
                .top-label{
                    position: absolute;
                    top: 0px;
                    white-space: nowrap;
                    color: #2e4c6e;
                    font-size: 14px;
                    font-weight: 900;
                    text-transform: capitalize;
                    display:none;
                }
                
                .slider-line{
                    border-bottom: 3px solid #2e4c6e;
                }
                .slider-circle-container{
                    position: relative;
                    margin-right: 30px !important;
                    top: 0;
                    left: 0;                    
                }
                .slider-circle{
                    width: 25px;
                    height: 25px;
                    border-radius: 15.5px;
                    background-color: white;
                    border: #2e4c6e solid 3px;
                    position: absolute;
                    top: -17px;

                }
                
                .bottom-label{
                    margin-top:15px;
                    text-transform:uppercase;
                    color:#6f8aaf;
                    font-size: 8px;
                    font-weight: 900;
                    
                }
                
                .left-label{
                    float: left;
                }
                
                .right-label{
                    float: right;
                }                
                
                /*PAGE 5 ******************************************************/
                
                .strategy-header{
                background-color: #1d3f65;
                padding: 10px 15px 13px 15px;
                color: white;
                letter-spacing: 0.5px;
                font-size: 14pt;
                line-height: 14pt;
                border-radius: 2px;
                margin-bottom: 40px;
                    display: inline-block;
                }
                
                /*PAGE 6*******************************************************/
                
                .lines{
                    border-bottom:1px solid rgba(33, 67, 104, 0.4);
                    height: 18pt;
                }
                
                .action-list .action{
                    height: 34pt;
                }
                
                .action-list .action .take{
                        width: 200pt;
                    border-bottom:1px solid rgba(33, 67, 104, 0.4);
                }
                
                .action-list .action .when{
                        width: 200pt;
                    border-bottom:1px solid rgba(33, 67, 104, 0.4);
                }
                
                .action-list .action .review{
                        width: 200pt;
                    border-bottom:1px solid rgba(33, 67, 104, 0.4);
                    
                }
                
                
                
                
                
        </style>     
        <script src="https://volpoll.org.au/api/public/reports/report.js"></script> 
    </head>

    <body>

        <htmlpagefooter name="page-footer" >   
            <table width="100%" class="footer-container">
                <tr>
                    <td width="50%" align="left" class="footer-date">{DATE j F Y}</td>
                    <td width="50%" align="right" class="footer-page">Page {PAGENO} of {nbpg}</td>
                </tr>
            </table>            
        </htmlpagefooter>  
    

            <?php 
                $constructs = array("safety", "sustainability", "satisfaction");
                foreach ($constructs as $construct){
                    

                    $count = 0;
                    foreach ($averages[$construct] as $intent=>$averageItems){
                        if ($intent !== "overall"){
                        ?>
                        
                            <htmlpagefooter name="no-display-<?php echo $construct . $count; ?>" style="display:none;">    
                                <div id="<?php echo $construct; ?>-container<?php echo $count; ?>" style="display:none;"></div>
                            </htmlpagefooter>      
                        
                        
    
                        <?php
                        $count++;
                        }
                        
                    }                  
   
                }
                
                
                $roles = array("volunteer", "leader");
                foreach ($roles as $role){
                    ?>
                    <htmlpagefooter name="no-display-<?php echo $role; ?>1" style="display:none;">    
                        <div id="<?php echo $role; ?>-container1" style="display:none;"></div>
                    </htmlpagefooter>    
                    <htmlpagefooter name="no-display-<?php echo $construct; ?>2" style="display:none;">    
                        <div id="<?php echo $role; ?>-container2" style="display:none;"></div>
                    </htmlpagefooter>        
            
                    <?php
                }                
                
            ?>



    
        <div class="first-page" name='first-page'>
            <div class="cover-container">
                <img class="cover-image" src="https://volpoll.org.au/api/public/reports/plus_cover_top.jpg"/>
                <div class="cover-text">
                    <div class="cover-date"><?php echo $currentDate; ?></div>
                    <div class="cover-title">Volunteering Victoria</div>
                    <div class="cover-sub">Organisation Summary</div>
                </div>
                <img class="cover-image" src="https://volpoll.org.au/api/public/reports/cover_bottom.jpg"/>
            </div>
        </div>    
    

        <div class="page">
            <img class="vnz-logo" src="https://volunteeringvictoria.org.au/volpoll/api/public/reports/vv_color.png"/>
            <div class="main-header"><span class="report-name">Organisation Summary</span></div>
            
            <div class="sub-header">See the results of your survey participants</div>
            <p>
                The Volunteering Victoria VolPoll tool is an online self-assessment looking at three domains that ensure effective volunteer involvement – sustainability, safety and satisfaction.
            </p>
            <p>
                For many Victorian organisations, the involvement of volunteers makes the difference between being able to achieve their mission and objectives, and not achieving them. Volunteers, therefore, are a precious resource.
            </p>
            <p>
                Volunteering doesn't happen in a vacuum; A lot of effort goes into supporting volunteering to ensure it has a powerful impact in supporting out community.               
            </p>
            <br><br>
            <div class="sub-header">What's in this report?</div>
            <p>This report is designed to provide a detailed summary of the responses to all the questions that have been answered by your users. It covers three main categories:</p>
            <table class="dimension-list">
                <tr class="dimension">
                    <td class="dimension-name">Satefy</td>
                    <td class="dimension-description">Seeing volunteers as a central part of the  organising and planning for how their efforts can  be best utilised to enable the organisation to achieve its goals</td>
                </tr>
                <tr class="dimension">
                    <td class="dimension-name">Sustainability</td>
                    <td class="dimension-description">The shared assumptions, values, and beliefs, which govern how people behave in the organisation</td>
                </tr>
                <tr class="dimension">
                    <td class="dimension-name">Satisfaction</td>
                    <td class="dimension-description">Having the appropriate type and level of tools and resources available to support volunteers and those who manage volunteers</td>
                </tr>              
            </table>            

        </div>      
    
    
        <div class="page">

            

            <div class="main-header">Results overview</div>

            <p>The radar chart below provides an overview of average score provided by each role in the organisation in each of the three categories – sustainability, safety and satisfaction. The further towards the edge of the spoke that is reached for each category the better it is performing in that category.</p>
            
            <br><br>
            <table class="polar-graph-container">
                
                <tr>
                    <td>
                        <div class="sub-header">Volunteers</div>
                        <div id="volunteer-svg"></div>
                    </td>
                    <td>
                        <div class="sub-header">Leaders</div>
                        <div id="leader-svg"></div>
                    </td>
                </tr>
            </table>
            
        </div>     
    
        

        <div class="page">
            <div class="main-header">Safety</div>
            
            <p>
                Safety is important for organisations. It is critical that leaders and volunteers know that everything is happening in a healthy and safe environment and covered from a legal and insurance perspective should anything go wrong. The questions reflect key aspects of volunteer safety including risk assessment, insurance, supervision, induction, training and conflict/issue resolution.
            </p>  
            
            <br>
            <div class="sub-header">Results (<?php echo round($averages["safety"]["overall"],2); ?> overall)</div>
            <div class='graph-container'>   
                
                <?php 
                    $count = 0;
                    foreach ($averages["safety"] as $intent=>$averageItems){
                        if ($intent !== "overall"){
                            echo '<div id="safety-svg' . $count . '"></div>';
                            $count++;
                        }
                        
                        
                    }
                ?>
            </div>

            
        </div>
    
    
    
    
    
    
        <div class="page">
            <div class="main-header">Sustainability</div>
            
            <p>
                Volunteer sustainability is important for organisations. IT is important that organisations consider the current and future capacity to ensure volunteer effort is not wasted. The questions reflect key aspects of volunteer sustainability such as current capacity, future capacity, retention, privacy, flexibility, procedure and recruitment.
            </p>  
            
            <br>
            <div class="sub-header">Results (<?php echo round($averages["sustainability"]["overall"],2); ?> overall)</div>
            <div class='graph-container'>  
                
                <?php 
                    $count = 0;
                    foreach ($averages["sustainability"] as $intent=>$averageItems){
                        if ($intent !== "overall"){
                            echo '<div id="sustainability-svg' . $count . '"></div>';
                            $count++;
                        }
                        
                        
                    }
                ?>           
            </div>
                      
            
        </div>   
    
    
    
        <div class="page">
            <div class="main-header">Satisfaction</div>
            
            <p>
                Volunteer satisfaction is critical for organisations. IT is key that organisations communicate well with their volunteers and help them to understand their community impact. These questions reflect key aspects of volunteer satisfaction such as communication, community alignment, recognition, inclusiveness, social opportunities, voice/democracy and net promoter score.
            </p>  
            
            <br>
            <div class="sub-header">Results (<?php echo round($averages["satisfaction"]["overall"],2); ?> overall)</div>
            <div class='graph-container'>          

                <?php 
                    $count = 0;
                    foreach ($averages["satisfaction"] as $intent=>$averageItems){
                        if ($intent !== "overall"){
                            echo '<div id="satisfaction-svg' . $count . '"></div>';
                            $count++;
                        }
                        
                        
                    }
                ?>                  
                
                
            </div>    
            
        </div>    
    

  
        

                <script>
 
                var averages = JSON.parse('<?php echo str_replace ("'","\'",$_POST["averages"]); ?>');

                var constructs = ["safety", "sustainability", "satisfaction"];
                
                var barChartConfig = function(){
                    return JSON.parse(JSON.stringify(
                    {
                        chart: {
                            type: 'bar',
                            backgroundColor: '#f4f5fa',
                            height:220,
                            width:800,
                            marginLeft: 170

                        },
                        title: {
                            text: '',
                            x: -80
                        },
                        plotOptions:{
                            column:{
                                groupPadding:0
                            },
                            series:{
                                groupPadding:0
                            }
                        },
                        xAxis: {
                            categories: ["","Volunteers","Leaders"],
                            lineWidth: 0,
                            labels: {
                                style: {
                                    width: '400px',
                                    'min-width': '400px',
                                    'max-width': '400px',
                                    useHTML : true
                                },
                            }

                        },

                        yAxis: {
                            min: 0,
                            max:5,
                            title: {
                                text: 'Average Score',
                                align: 'high'
                            },
                            labels: {
                                overflow: 'justify'
                            }
                        },
                        tooltip: {
                            valueSuffix: ' points'
                        },

                        legend: {
                            align: 'right',
                            verticalAlign: 'top',
                            y: 70,
                            layout: 'vertical',
                            enabled:false
                        },

                        series: [{
                            name: 'Your result',
                            data: [
                                {y:0, color:"#25476D"},
                                {y:0, color:"#98ACC3"},
                                {y:0, color:"#98ACC3"}
                                ]
                            }                   
                        ],
                        credits: {
                            enabled: false
                        }                
                    }  
                    ))
                }
                
                
                var intents = {}
                
                for (var index in averages){
                    intents[index] = [];
                    for (var index2 in averages[index]){
                        if (index2 !== "overall"){
                            intents[index].push(index2);
                        }
                    }
                }
                
                
                var charts = {};
                
                for (var index in constructs){
                    var construct = constructs[index];
                    
                    charts[construct] = [];
                    
                    var count = 0;
                    for (var index2 in averages[construct]){
                        if (index2 !== "overall"){
                            
                            var config = barChartConfig();
                            config.xAxis.categories[0] = index2;
                            config.series[0].data[0].y = averages[construct][index2] ? averages[construct][index2]["overall"] : 0;
                            config.series[0].data[1].y = averages[construct][index2] ? averages[construct][index2]["volunteer"] : 0;
                            config.series[0].data[2].y = averages[construct][index2] ? averages[construct][index2]["leader"] : 0;   
                            
                            charts[construct].push(Highcharts.chart(construct + '-container' + count, config));
                            $("#" + construct + "-svg" + count).html(charts[construct][count].getSVG());
                            
                            count++;
                        }
                    }
                    

                    
                }
                
                
                
                
                
                
                var polarAverages = JSON.parse('<?php echo $_POST["polarAverages"]; ?>');
                
                
                var polarChartConfig = function(){
                    return JSON.parse(JSON.stringify(
                    {

                        chart: {
                            polar: true,
                            type: 'line',
                            backgroundColor: '#f4f5fa',
                            width:400,
                            height:250

                        },

                        title: {
                            text: '',
                            x: -80
                        },

                        pane: {
                            size: '80%'
                        },

                        xAxis: {
                            categories: ["Safety","Sustainability", "Satisfaction"],
                            tickmarkPlacement: 'on',
                                        tickInterval:1,
                            lineWidth: 0,
                            min: 0,
                            max:3,
                            angle:45

                        },

                        yAxis: [
                            {
                            gridLineInterpolation: 'polygon',
                            showLastLabel: true,
                            lineWidth: 0,
                            tickInterval:1,
                            min: 0,
                            pane:0,
                            max:5,
                            angle:-45
                            },
                            {
                            showLastLabel: true,
                            gridLineWidth: false,                        
                            lineWidth: 0,
                            tickInterval:1,
                            min: 0,
                            pane:1,
                            max:5,
                            verticalAlign: 'bottom',
                            angle:0
                            }                         
                        ],
                        pane:[{startAngle:45},{startAngle:90}],
                        tooltip: {
                            shared: true,
                            pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.0f}</b><br/>'
                        },

                        legend: {
                            align: 'right',
                            verticalAlign: 'top',
                            y: 70,
                            layout: 'vertical',
                            enabled:false
                        },

                        series: [{
                            name: 'Result',
                            data: [],
                            pointPlacement: 'on'
                            },
                            {
                                yAxis: 1,
                                data: [],
                                visible: false
                            }                    
                        ],
                        credits: {
                            enabled: false
                        }                

                    })
                        )                
                }
                
                for (var index in polarAverages){
                
                    var averages = polarAverages[index];
                
                    var role = index;
                    
                    charts[role] = [];
                    
                    var config = polarChartConfig();
                    
                    for (var index2 in averages){
                        config.series[0].data.push(averages[index2]);
                    }
 
                    
                    
                    charts[role] = Highcharts.chart(role.replace(" ", "-") + '-container1', config);
                    
                    $("#" + role.replace(" ", "-") + "-svg").html(charts[role].getSVG());              
                }

				
				

                </script>


    </body>
</html>