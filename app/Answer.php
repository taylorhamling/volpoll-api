<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $fillable = [
        'user_id', 'field_id', 'answer', 'skipped', 'text_answer'
    ];
}
