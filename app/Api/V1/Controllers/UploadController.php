<?php
namespace App\Api\V1\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use JWTAuth;
use Dingo\Api\Routing\Helpers;


class UploadController extends Controller
{
    use Helpers;

    
    public function upload(Request $request)
    {
        //$currentUser = JWTAuth::parseToken()->authenticate();
        
      
        if ($request->hasFile('media_file')) {
            $mediaFile = $_FILES['media_file'];
            $name = md5(time()) . basename($mediaFile["name"]);
            move_uploaded_file($mediaFile["tmp_name"], "uploads/" . $name);
            
            return url('/') . '/uploads/' . $name;
        }       

        return $this->response->error('could_not_upload_file', 500);        
    } 
    

    
}
