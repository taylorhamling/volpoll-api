<?php

namespace App\Api\V1\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use JWTAuth;
use App\User;
use App\Profile;
use App\Field;
use App\Answer;
use App\Role;
use App\FieldGroup;
use App\Report;
use Dingo\Api\Routing\Helpers;

class AdminController extends Controller
{
    use Helpers;
    public function view(Request $request)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();  
        if ($currentUser->permission !== "admin"){
            //throw new NotFoundHttpException;
        }
        
        $search = $request->get('search') ? $request->get('search') : "";
        $limit = $request->get('limit') ? $request->get('limit') : 50;
        $offset = $request->get('page') ? ($request->get('page') * $limit) - $limit : 0;
        
        $order = $request->get('order') ? $request->get('order') : 'created_at';
        $orderDirection = "DESC";
        
        $submissions = Profile::
                where(function($query) use ($search){
                    $query->where("first_name", "like", "%" . $search . "%")
                          ->orWhere("last_name", "like", "%" . $search . "%")
                          ->orWhere('email', "like", "%" . $search . "%");
                })
                ->orderBy($order, $orderDirection)
                ->skip($offset)->take($limit)                  
                ->get();    
                
        foreach ($submissions as $submission){
            $role = Role::where("id", $submission->role_id)->first();
            $submission["role"] = $role;
            
            
            $answers = Answer::where("user_id", $submission->user_id)->orderBy("created_at", "ASC")->get();

            foreach ($answers as $key=>$answer){
                $field = Field::where("id", $answer->field_id)->first();
                if ($field){
                $fieldGroup = FieldGroup::where("id", $field->field_group_id)->first();
                
                $answers[$key]["field"] = $field;
                $answers[$key]["field_group"] = $fieldGroup;
                }
                $answers[$key]["average"] = Answer::where("field_id", $answer->field_id)->whereNotNull("answer")->avg('answer');


            }  

            $submission["answers"] = $answers;

            
        }        
                
                

        $count = Profile::
                where(function($query) use ($search){
                    $query->where("first_name", "like", "%" . $search . "%")
                          ->orWhere("last_name", "like", "%" . $search . "%")
                          ->orWhere('email', "like", "%" . $search . "%");
                })              
                ->count();     
        
        return array("count"=>$count, "data"=>$submissions);

        
    }
    
 
    
    
    public function export($id)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();

        if ($currentUser->permission !== "admin"){
            //throw new NotFoundHttpException;
        }        
        
        $submission = Profile::where('id', $id)->first();
        if(!$submission){
            throw new NotFoundHttpException; 
        }

               
        
        $answers = Answer::where("user_id", $submission->user_id)->orderBy("created_at", "ASC")->get();
        
        foreach ($answers as $key=>$answer){
            $field = Field::where("id", $answer->field_id)->first();
            $answers[$key]["field"] = $field;
            $answers[$key]["average"] = Answer::where("field_id", $answer->field_id)->whereNotNull("answer")->avg('answer');

        }        
        

        
        $fileName = 'exports/submission' . md5($submission->id . time())  . '.csv';
        
        $file = fopen($fileName,"w");

        
        
        fputcsv($file, array("Role", "Section", "Question", "Answer", "Average"));

        foreach($answers as $answer){

            $formattedAnswer = array(
                $answer["field"]->role,$answer["field"]->section, $answer["field"]->question, 
                $answer->answer, $answer["average"]         
            );
            fputcsv($file, $formattedAnswer);
        }    
        
        
        return array("file" => url('/') . '/' . $fileName); 
   
    }
    
    public function exportall()
    {
        $currentUser = JWTAuth::parseToken()->authenticate();

        if ($currentUser->permission !== "admin"){
            //throw new NotFoundHttpException;
        }        

        $submissions = Profile::get();  

 
        $fileName = 'exports/submissions.csv';
        
        $file = fopen($fileName,"w");        
        fputcsv($file, array(
            "ID","First Name","Last Name","Group", "Email", 
            "Role", "Created At"));        
        foreach($submissions as $submission){
            
            
            $role = Role::where("id", $submission->role_id)->first();
            
            $formattedSubmission = array(
                $submission->id, $submission->first_name,$submission->last_name,$submission->group, $submission->email, 
                $role->name, $submission->created_at          
            );
            fputcsv($file, $formattedSubmission);
        }
        
        return array("file" => url('/') . '/' . $fileName);     

    }     
    
    
    public function exportanswers()
    {
        $currentUser = JWTAuth::parseToken()->authenticate();

        if ($currentUser->permission !== "admin"){
            //throw new NotFoundHttpException;
        }        

        $submissions = Profile::get();  

 
        $fileName = 'exports/answers.csv';
        
        $file = fopen($fileName,"w");        
        fputcsv($file, array(
            "ID","First Name","Last Name","Role", "Field Group", "Question", "Answer", "Average"));        
        foreach($submissions as $submission){
            
            $role = Role::where("id", $submission->role_id)->first();
            
            $answers = Answer::where("user_id", $submission->user_id)->orderBy("created_at", "ASC")->get();

            foreach ($answers as $key=>$answer){
                $field = Field::where("id", $answer->field_id)->first();
                if ($field){
                    $fieldGroup = FieldGroup::where("id", $field->field_group_id)->first();
                    $answers[$key]["field"] = $field;
                    $answers[$key]["field_group"] = $fieldGroup;
                    $answers[$key]["average"] = Answer::where("field_id", $answer->field_id)->whereNotNull("answer")->avg('answer');

                    $formattedAnswer = array(
                        $answer->id,$submission->first_name, $submission->last_name, $role->name, $fieldGroup->name,
                        $field->question, $answer->answer, $answers[$key]["average"]       
                    );                

                    fputcsv($file, $formattedAnswer);
                }

            }             

            
        }
        
        return array("file" => url('/') . '/' . $fileName);     

    }  
    
    
    public function exportaverages()
    {
        $currentUser = JWTAuth::parseToken()->authenticate();

        if ($currentUser->permission !== "admin"){
            //throw new NotFoundHttpException;
        }        

        $fields = Field::where("type", "Select")->get();  
        
        $headers = array( "ID","Field Group", "Question", "Average");
            
      
        
        
 
        $fileName = 'exports/averages.csv';
        
        $file = fopen($fileName,"w");    
        
        
        
        
      
        
        
        fputcsv($file, $headers);        
        foreach($fields as $field){
            
            //$role = Role::where("id", $submission->role_id)->first();

            $fieldGroup = FieldGroup::where("id", $field->field_group_id)->first();
            $average = Answer::where("field_id", $field->id)->whereNotNull("answer")->avg('answer');


            
            
            if ($average && $average > 0){
            
            $formattedAnswer = array(
                $field->id,$fieldGroup->name, $field->question, $average    
            );                
            

            
            

            fputcsv($file, $formattedAnswer);
            }

            
        }

        
        return array("file" => url('/') . '/' . $fileName);     

    }    
    
    
    
    public function destroy($id)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();

        $profile = Profile::find($id);

        if(!$profile)
            throw new NotFoundHttpException;

        
        $userId = $profile->user_id;
        
        if($profile->delete()){
            
            Answer::where("user_id", $userId)->delete();
            Report::where("user_id", $userId)->delete();
            return $this->response->noContent();
        }
        else
            return $this->response->error('could_not_delete_profile', 500);        
    }      
    
}


