<?php

namespace App\Api\V1\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use JWTAuth;
use App\User;
use App\Profile;
use App\Field;
use App\FieldGroup;
use App\Role;
use Dingo\Api\Routing\Helpers;

class FieldController extends Controller
{
    use Helpers;
    public function index(Request $request)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        
        $roleId = $request->get("role_id");
        
        if ($roleId){
            return $this->getFields($roleId);        
            
            
        }
        
        else{
          $roles = Role::get();
          $fields = array();
          
          foreach ($roles as $role){
              $fields[$role->id] = $this->getFields($role->id);
          }
          
          return $fields;
          
        }

    } 
    
    
    public function count(Request $request)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        
        $profile = Profile::where("user_id", $currentUser->id)->first();
        
        $roleId = 1;
        if ($profile){
            $roleId = $profile->role_id;
        }    
        
        return Field::where("role_id", $roleId)->count();        


    }     
    
    
    private function getFields($roleId){
        $fields = Field::where("role_id", $roleId)->orderBy("field_order", "ASC")->get();

        foreach ($fields as $key => $field){
            $fields[$key]["field_group"] = FieldGroup::find($field->field_group_id);  
            $fields[$key]["role"] = Role::find($field->role_id);    
        }  

        return $fields;              
    }
    
    
    
    
    
    public function store(Request $request)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        
        $field = new Field($request->all());
        
        
        if ($request->get("field_group_id") === "create"){
            $fieldGroup = new FieldGroup;
            $fieldGroup->name = $request->get("new_group");
            $fieldGroup->save();
            $field->field_group_id = $fieldGroup->id;
        }
        

        if($field->save()){
            return $field;
        }
        else
            return $this->response->error('could_not_create_field', 500);        
    } 

    
    
    public function show($id)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        
        $profile = Profile::where("user_id", $currentUser->id)->first();
        
        $roleId = 1;
        if ($profile){
            $roleId = $profile->role_id;
        }

        $field = Field::where("field_order", $id)->where("role_id", $roleId)->first();

        if(!$field)
            throw new NotFoundHttpException; 
        
        $field["field_group"] = FieldGroup::find($field->field_group_id);  
        $field["role"] = Role::find($field->role_id);           

        return $field;        
    }    
    
    public function update(Request $request, $id)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();

        $field = Field::find($id);
        if(!$field)
            throw new NotFoundHttpException;

            $field->fill($request->all());

            if($field->save()){

                return $this->response->noContent();
            }
            else
                return $this->response->error('could_not_update_field', 500);   
        
    }
    

    public function destroy($id)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();

        $field = Field::find($id);

        if(!$field)
            throw new NotFoundHttpException;

        
        if($field->delete())
            return $this->response->noContent();
        else
            return $this->response->error('could_not_delete_field', 500);        
    }   
    
    
    public function reorderFields(Request $request)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();

        $fields = $request->get("fields");
        
        if(!$fields)
            throw new NotFoundHttpException;
        
        
        foreach ($fields as $field){
            $updateField = Field::find($field["id"]);
            if ($updateField){
                $updateField->field_order = $field["field_order"];
                $updateField->save();
            }
        }
        
    }    
    
    
    
    public function fieldGroups(Request $request)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        
        $fieldGroups = FieldGroup::orderBy("field_group_order", "ASC")->get();
 
        
        return $fieldGroups;
    }     
}
