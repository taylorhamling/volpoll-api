<?php

namespace App\Api\V1\Controllers;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\LoginRequest;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Auth;

use Illuminate\Http\Request;

use App\User;
use App\Profile;
use App\Question;
use App\Answer;
use App\Field;
use App\Report;

use DB;

use Mail;

class UserController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt.auth', []);
    }

    
    public function index(Request $request)
    {
        $currentUser = Auth::guard()->user();
        if ($currentUser->permission !== "admin"){
            //throw new NotFoundHttpException;
        }
        
        $search = $request->get('search') ? $request->get('search') : "";

        $users = User::
                where(function($query) use ($search){
                    $query->where("email", "like", "%" . $search . "%")
                          ->orWhere('id', 'like', $search);
                })   
                ->where(function($query){
                    $query->where("permission", "admin")
                          ->orWhere("permission", "sadmin")
                            ->orWhere("permission", "roadmin");
                })                 
                ->get();        
            
                
                
                
                
        
        return $users;
    }



    public function store(Request $request)
    {
        $currentUser = Auth::guard()->user();
        if ($currentUser->permission !== "admin"){
            //throw new NotFoundHttpException;
        }
        
        $email = $request->get("email");
        
        $password = str_random(20);
        $userData = array(
            "email" => $email,
            "permission" => "admin",
            "password" => $password   
        );
                

        User::unguard();
        $user = User::create($userData);
        User::reguard();

        if(!$user->id) {
            return $this->response->error('could_not_create_user', 500);
        }
        
        Mail::send('emails.adminuser', ["email"=>$email, "password"=>$password], function($message) use ($email)
        {
            $message->to($email)->subject("New admin account for VolPoll");
        });            
        

        return $user;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        if ($currentUser->permission !== "admin"){
            //throw new NotFoundHttpException;
        }
        
        $user = User::find($id);

        if(!$user)
            throw new NotFoundHttpException; 

        return $user;
    }


    public function update(Request $request, $id)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        if ($currentUser->permission !== "admin"){
            //throw new NotFoundHttpException;
        }
        
        $user = User::find($id);
        if(!$user)
            throw new NotFoundHttpException;

        $user->fill($request->all());

        if($user->save())
            return $this->response->noContent();
        else
            return $this->response->error('could_not_update_user', 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $currentUser = Auth::guard()->user();
        if ($currentUser->permission !== "admin"){
            //throw new NotFoundHttpException;
        }
        
        
        $user = User::find($id);
        if(!$user)
            throw new NotFoundHttpException;


        if($user->delete())
            return $this->response->noContent();
        else
            return $this->response->error('could_not_delete_user', 500);
    }    
    
    
    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        $user = Auth::guard()->user();
        $user["profile"] = Profile::where("user_id", $user->id)->first();
        $latestAnswer = Answer::where("user_id", $user->id)->orderBy("updated_at", "DESC")->first();

        if ($latestAnswer){
            $user["active_question"] = Field::find($latestAnswer->field_id);
        }
        $user["completed"] = Report::where("user_id", $user->id)->orderBy("updated_at", "DESC")->first();
        
        
        return $user;
    }
    
    public function updateme(Request $request)
    {
        $currentUser = Auth::guard()->user();

        $profile = Profile::where("user_id", $currentUser->id)->first();
        if(!$profile)
            throw new NotFoundHttpException;

        $profile->fill($request->all());

        if($profile->save()){
            
            if ($request->get("password")){
                DB::table('users')->where('email', $user->email)->update(['password'=> bcrypt($request->get("password"))]);          
            }
            
            
            return;
        }
        else
            //return $this->response->error('could_not_update_user', 500);
            throw new NotFoundHttpException;
    }
    
    
    
    
    
}
