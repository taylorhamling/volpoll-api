<?php

namespace App\Api\V1\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use JWTAuth;
use App\Submission;
use App\Report;
use App\Answer;
use App\Field;
use App\Setting;

use Dingo\Api\Routing\Helpers;
use Mail;

use DB;

class SettingController extends Controller
{
    use Helpers;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$currentUser = JWTAuth::parseToken()->authenticate();
        return Setting::
            orderBy('id', 'ASC')
            ->get()
            ->toArray();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();

        $setting = new Setting;

        $setting->key = $request->get('key');
        $setting->data = $request->get('data');

        if($currentUser->settings()->save($setting))
            return $setting;
        else
            return $this->response->error('could_not_create_setting', 500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $field = Setting::where("key", $id)->first();

        if(!$field)
            throw new NotFoundHttpException; 

        return $field;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();

        $setting = Setting::where("key", $request->get("key"))->first();
        if(!$setting)
            throw new NotFoundHttpException;

        $setting->fill($request->all());

        if($setting->save())
            return $this->response->noContent();
        else
            return $this->response->error('could_not_update_setting', 500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();

        $setting = Setting::find($id);

        if(!$setting)
            throw new NotFoundHttpException;

        if($setting->delete())
            return $this->response->noContent();
        else
            return $this->response->error('could_not_delete_setting', 500);
    }
}
