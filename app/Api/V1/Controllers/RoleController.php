<?php

namespace App\Api\V1\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use JWTAuth;
use App\User;
use App\Field;
use App\FieldGroup;
use App\Role;
use Dingo\Api\Routing\Helpers;

class RoleController extends Controller
{
    use Helpers;
    public function index(Request $request)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        
        $roles = Role::orderBy("role_order", "ASC")->get();
        
        return $roles;
    } 
    
 
}
