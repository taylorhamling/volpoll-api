<?php

namespace App\Api\V1\Controllers;

use Config;
use App\User;
use App\Profile;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\SignUpRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;
use DB;

class SignUpController extends Controller
{
    public function signUp(SignUpRequest $request, JWTAuth $JWTAuth)
    {
        $user = new User($request->all());
        
        $userCount = DB::table("users")->count();
        
        $user->email = $userCount . "@volpoll.com.au";
        $user->password = 'volpoll1234';
        
        if(!$user->save()) {
            throw new HttpException(500);
        }

        
        $profile = new Profile($request->all());

        
        $user->profiles()->save($profile);            
        
        /*
        if(!Config::get('boilerplate.sign_up.release_token')) {
            return response()->json([
                'status' => 'ok'
            ], 201);
        }
        * */

        $token = $JWTAuth->fromUser($user);
        return response()->json([
            'status' => 'ok',
            'token' => $token
        ], 201);
    }
}
