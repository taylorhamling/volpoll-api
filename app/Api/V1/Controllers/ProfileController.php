<?php

namespace App\Api\V1\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use JWTAuth;
use App\User;
use App\Profile;
use Dingo\Api\Routing\Helpers;

class ProfileController extends Controller
{
    use Helpers;
    public function index(Request $request)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        
        return $currentUser->profiles()->first();
    } 
    
    public function store(Request $request)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        
        $profile = new Profile($request->all());


        if($currentUser->profiles()->save($profile)){
           
 
            return $this->response->created();
        }
        else
            return $this->response->error('could_not_create_profile', 500);        
    } 

    
    
    public function show($id)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();

        $profile = Profile::find($id);

        if(!$profile)
            throw new NotFoundHttpException; 

        return $profile;        
    }    
    
    public function update(Request $request, $id)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();

        $profile = Profile::find($id);
        if(!$profile)
            throw new NotFoundHttpException;
        
        if ($request->get("email")){
            $user = User::find($currentUser->id);
            if($user){
                $user->fill($request->all()); 
                $user->save();
            }
        }
        

        $profile->fill($request->all());

        if($profile->save()){

            return $this->response->noContent();
        }
        else
            return $this->response->error('could_not_update_profile', 500);        
    }

   
    

    public function destroy($id)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();

        $profile = Profile::find($id);

        if(!$profile)
            throw new NotFoundHttpException;

        
        if($profile->delete())
            return $this->response->noContent();
        else
            return $this->response->error('could_not_delete_profile', 500);        
    }   
    
    
    
}


