<?php

namespace App\Api\V1\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use JWTAuth;
use App\User;
use App\Answer;
use App\Profile;
use App\Report;
use App\Field;
use App\FieldGroup;

use Dingo\Api\Routing\Helpers;

class StatsController extends Controller
{
    use Helpers;
    public function index(Request $request)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        
        $submissionCount = Profile::count();
        $leaders = Profile::where("role_id", 1)->count();
        $volunteers = Profile::where("role_id", 2)->count();
        $completed = Report::count();
        $notCompleted = $submissionCount - $completed;
        $answerCount = Answer::count();
        $answers = Answer::get();
        
        $profiles = Profile::orderBy("created_at", "ASC")->get();
        $chartData = array();
        $chartDataTotal = array();
        
        $submissionTotal = 0;
        foreach ($profiles as $profile){
            $submissionTotal += 1;
            $date = date('Y-m-d', strtotime($profile->created_at));
            if (array_key_exists($date, $chartData)){
                $chartData[$date] = $chartData[$date] + 1;
                
            }
            else{
                $chartData[$date] = 1;
            }
            
            $chartDataTotal[$date] = $submissionTotal;
        }
        
        $chartDataFormatted = array();
        foreach($chartData as $x => $y){
            array_push($chartDataFormatted, array("x" => $x, "y" => $y));
        }
        
        $chartDataTotalFormatted = array();
        foreach($chartDataTotal as $x => $y){
            array_push($chartDataTotalFormatted, array("x" => $x, "y" => $y));
        }   
        
        
        $polar = array(
            "leaders" => 
                array("sustainability" => 0, "sustainability_count" => 0, "safety" => 0, "safety_count" => 0, "satisfaction" => 0, "satisfaction_count" => 0), 
            "volunteers" => 
                array("sustainability" => 0, "sustainability_count" => 0, "safety" => 0, "safety_count" => 0, "satisfaction" => 0, "satisfaction_count" => 0)
            );
        
        $safetyFields = Field::where("field_group_id", 3)->pluck("id")->toArray();
        $satisfactionFields = Field::where("field_group_id", 4)->pluck("id")->toArray();
        $sustainabilityFields = Field::where("field_group_id", 1)->pluck("id")->toArray();

        
        $leaderIds = Profile::where("role_id", 1)->pluck("user_id")->toArray();
        $volunteerIds = Profile::where("role_id", 2)->pluck("user_id")->toArray();
        
        foreach ($answers as $answer){
            
            if ($leaderIds && in_array($answer->user_id, $leaderIds)){
            
                if ($safetyFields && in_array($answer->field_id, $safetyFields) && $answer->skipped !== 1){
                    $polar["leaders"]["safety"] += (int) $answer->answer;
                    $polar["leaders"]["safety_count"] += 1;
                }
                else if ($sustainabilityFields && in_array($answer->field_id, $sustainabilityFields) && $answer->skipped !== 1){
                    $polar["leaders"]["sustainability"] += (int) $answer->answer;
                    $polar["leaders"]["sustainability_count"] += 1;
                }   
                else if ($satisfactionFields && in_array($answer->field_id, $satisfactionFields) && $answer->skipped !== 1){
                    $polar["leaders"]["satisfaction"] += (int) $answer->answer;
                    $polar["leaders"]["satisfaction_count"] += 1;
                }              
            
            }
            
            
            else if ($volunteerIds && in_array($answer->user_id, $volunteerIds)){
            
                if ($safetyFields && in_array($answer->field_id, $safetyFields) && $answer->skipped !== 1){
                    $polar["volunteers"]["safety"] += (int) $answer->answer;
                    $polar["volunteers"]["safety_count"] += 1;
                }
                else if ($sustainabilityFields && in_array($answer->field_id, $sustainabilityFields) && $answer->skipped !== 1){
                    $polar["volunteers"]["sustainability"] += (int) $answer->answer;
                    $polar["volunteers"]["sustainability_count"] += 1;
                }   
                else if ($satisfactionFields && in_array($answer->field_id, $satisfactionFields) && $answer->skipped !== 1){
                    $polar["volunteers"]["satisfaction"] += (int) $answer->answer;
                    $polar["volunteers"]["satisfaction_count"] += 1;
                }               
            
            }            
            
            
        }
        
        $polar["leaders"]["safety"] = $polar["leaders"]["safety"] > 0 ? ($polar["leaders"]["safety"] / $polar["leaders"]["safety_count"]) : 0;
        $polar["leaders"]["sustainability"] = $polar["leaders"]["sustainability"] > 0 ? ($polar["leaders"]["sustainability"] / $polar["leaders"]["sustainability_count"]) : 0;
        $polar["leaders"]["satisfaction"] = $polar["leaders"]["satisfaction"] > 0 ? ($polar["leaders"]["satisfaction"] / $polar["leaders"]["satisfaction_count"]) : 0;
        
        $polar["volunteers"]["safety"] = $polar["volunteers"]["safety"] > 0 ? ($polar["volunteers"]["safety"] / $polar["volunteers"]["safety_count"]) : 0;
        $polar["volunteers"]["sustainability"] = $polar["volunteers"]["sustainability"] > 0 ? ($polar["volunteers"]["sustainability"] / $polar["volunteers"]["sustainability_count"]) : 0;
        $polar["volunteers"]["satisfaction"] = $polar["volunteers"]["satisfaction"] > 0 ? ($polar["volunteers"]["satisfaction"] / $polar["volunteers"]["satisfaction_count"]) : 0;        
        
        
        
        
        
        return array(
            "submissions" => $submissionCount,
            "answers" => $answerCount,
            "leader_submissions" => $leaders,
            "volunteer_submissions" => $volunteers,
            "completed" => $completed,
            "not_completed" => $notCompleted,
            "submissions_chart" => $chartDataFormatted,
            "submissions_accumulated_chart" => $chartDataTotalFormatted,
            "polar_chart" => $polar
            );
        
    } 
    
    public function show(Request $request, $id)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        
    }     
    
}
