<?php

namespace App\Api\V1\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use JWTAuth;
use App\User;
use App\Answer;
use App\Field;
use App\FieldGroup;
use App\Role;
use Dingo\Api\Routing\Helpers;

class AnswerController extends Controller
{
    use Helpers;
    public function index(Request $request)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        
        $answers = $currentUser->answers()->get();
        
        foreach ($answers as $key => $answer){
            $field = Field::find($answer->field_id); 
            $answers[$key]["field"] = $field;
            if ($field){
                $answers[$key]["field_group"] = FieldGroup::find($field->field_group_id);  
                $answers[$key]["role"] = Role::find($field->role_id);  
            }
            
        }  
        
        return $answers;
    } 
    
    public function store(Request $request)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        
        
        $answerId = $request->get("id");
        
        if ($answerId){
            //update           
            $answer = Answer::find($answerId);
            if($answer){

                $answer->fill($request->all());

                if($answer->save()){

                    return $answer;
                }
                else
                    return $this->response->error('could_not_update_answer', 500);   
                
            }
            
        }
        
        
        $answer = new Answer($request->all());


        if($currentUser->answers()->save($answer)){
            return $answer;
        }
        else
            return $this->response->error('could_not_create_answer', 500); 
        
        
    } 

    
    
    public function show($id)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();

        $answer = Answer::where("field_id", $id)->where("user_id", $currentUser->id)->first();

        if(!$answer)
            throw new NotFoundHttpException; 
        
        $field = Field::find($answer->field_id);
        $answer["field"] = $field; 
        if ($answer["field"]){
            $answer["field_group"] = FieldGroup::find($field->field_group_id);  
            $answer["role"] = Role::find($field->role_id);  
        }        

        return $answer;        
    }    
    
    public function update(Request $request, $id)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();

        $answer = Answer::find($id);
        if(!$answer)
            throw new NotFoundHttpException;

            $answer->fill($request->all());

            if($answer->save()){

                return $this->response->noContent();
            }
            else
                return $this->response->error('could_not_update_answer', 500);   
        
    }
    

    public function destroy($id)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();

        $answer = Answer::find($id);

        if(!$answer)
            throw new NotFoundHttpException;

        
        if($answer->delete())
            return $this->response->noContent();
        else
            return $this->response->error('could_not_delete_answer', 500);        
    }   
    
    
    
}
