<?php

namespace App\Api\V1\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use JWTAuth;
use App\User;
use App\Report;
use App\Field;
use App\Answer;
use App\FieldGroup;
use App\Role;
use App\Profile;
use Dingo\Api\Routing\Helpers;


use Mail;

use PDF;

use DB;

use PhantomJs;


class ReportController extends Controller
{
    use Helpers;
	
	public function sendTestEmail(){
       $email = "sendgridtesting@gmail.com";
        //send email
        Mail::send('emails.test', [], function($message) use ($email)
        {
            $message->to($email)->subject("CC044646E5A8");
        }); 		
	}
	
	
    public function index(Request $request)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        
        return $currentUser->reports()->first();
    } 
    
    public function store(Request $request)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();
        
        $report = new Report($request->all());


        if($currentUser->reports()->save($report)){
           
 
            return $this->response->created();
        }
        else
            return $this->response->error('could_not_create_report', 500);        
    } 

    
    
    public function show(Request $request)
    {
        error_reporting(0);
        @ini_set('display_errors', 0);
        
        $currentUser = JWTAuth::parseToken()->authenticate();

        $report = Report::where("user_id", $currentUser->id)->first();

        if(!$report){
            
            $report = new Report;
            $report->url = str_random(20);
            
            $currentUser->reports()->save($report);
            
            $reportData = $this->getReportData($report);
            
            $this->generatePDF($reportData);
            
            $this->email($reportData["report"], $reportData["profile"]);
            
            
            $profile = $reportData["profile"];
            $position = $profile["position"] ? strtolower($profile["position"]) : "";
            if ($profile["role_id"] === 1 && (
                    strcmp($position, "ceo") === 0 ||
                    strcmp($position, "vice president") === 0  ||
                    strcmp($position, "president") === 0  ||
                    strcmp($position, "board member") === 0  ||
                    strcmp($position, "chair") === 0 
                    )){
                
                
                $this->managementEmail($profile);
                
            }
            
            
            return $report;
            
        }
            

        return $report;        
    }  
    
    public function getPDF(Request $request, $url){

        $report = Report::where("url", $url)->first();
        
        if (!$report)
            throw new NotFoundHttpException;
        
        
        if (!$report->file){
            $reportData = $this->getReportData($report);

            //$this->email($reportData["report"], $reportData["profile"]);
            $this->generatePDF($reportData);            
        }
        
        header("Content-type: application/pdf");
        header("Content-Disposition: inline; filename=" . $report->file);
        @readfile($report->file);
        
        //return $report;
    }
    
    
    public function getReportHtml(Request $request){
        
        $currentUser = JWTAuth::parseToken()->authenticate();

        $report = Report::where("user_id", $currentUser->id)->first();

        if(!$report){
            
            $report = new Report;
            $report->url = str_random(20);
            
            $currentUser->reports()->save($report);

            
        }   
		
		

        
        if (!$report->file){
            
        
            
            $reportData = $this->getReportData($report);
            
            $curlSafeReportData = array();
            
            foreach ($reportData as $key => $item){
                $curlSafeReportData[$key] = json_encode($item);
            }
            
            
            
            
            $ch = curl_init('https://volpoll.org.au/api/resources/views/pdfs/report.php?report=' . $report->url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_BINARYTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $curlSafeReportData);            

            
   

            $response = curl_exec ($ch);
            
            curl_close ($ch);
            
            
            return array("html" => base64_encode($response), "report" => $report);            
            
            
        }
        else{
            
            return array("file" => $report->file, "report" => $report);
            
            
        }

    }
    
    public function generatePdfFromHtml(Request $request){
        error_reporting(0);
        @ini_set('display_errors', 0);
        
        $currentUser = JWTAuth::parseToken()->authenticate();

        $report = Report::where("user_id", $currentUser->id)->first();
        
        if (!$report)
            throw new NotFoundHttpException;        
        
        
        $html = base64_decode($request->get("html")); //str_replace('\"', '"', str_replace("\n", "", $request->get("html")));
        
		$html = str_replace("\n", "", $html);
     
        //return array("html" => $html);die();
        
        
        $pdf = PDF::loadHTML($html);


        $pdf->save('reports/VolPollReport' . $report->url . '.pdf');  


        $report->file = 'reports/VolPollReport' . $report->url . '.pdf';
        $report->save();   
        
        
        return array("file" => $report->file);
      
        
    }
    
    
    private function getReportData($report){
        $userId = $report->user_id;
        
        $profile = Profile::where("user_id", $userId)->first()->toArray();
        $profile["role"] = Role::where("id", $profile["role_id"])->first()->toArray();
        
        $fieldGroups = FieldGroup::orderBy("field_group_order", "ASC")->get()->toArray();
        
        $answers = Answer::where("user_id", $userId)->get()->toArray();
        
        
        foreach ($answers as $key => $answer){
            $field = Field::where("id", $answer["field_id"])->first()->toArray();
            $answers[$key]["field"] = $field;
            //$answers[$key]["field"]["field_group"] = FieldGroup::find($field->field_group_id);  
            //$answers[$key]["field"]["role"] = Role::find($field->role_id);             
        }  
        
        $data = array(
                    "answers" => $answers,
                    "profile" => $profile,
                    "report" => $report->toArray(),
                    "field_groups" => $fieldGroups
                );
        
        return $data;
    }
    
    
    
    private function generatePDF($reportData){
        

        
        $report = $reportData["report"];
        
        $request = \PhantomJs::post('https://volpoll.org.au/api/resources/views/pdfs/report.php?report=' . $report->url);
        $request->setRequestData($reportData);
        $request->setDelay(5);
        
        $response = \PhantomJs::isLazy()->send($request);
        
        //print_r($response->getContent());
        //die();


        if($response->getStatus() === 200 || $response->getStatus() === 301) {

            // Dump the requested page content
            $pdf = PDF::loadHTML($response->getContent());

            
            $pdf->save('reports/VolPollReport' . $report["url"] . '.pdf');  

            
            $report->file = 'reports/VolPollReport' . $report["url"] . '.pdf';
            $report->save();
            
            //$pdf->stream('VolPollReport' . $report["id"] . '.pdf');  

        }
        else{
            throw new NotFoundHttpException;
        }
         
    } 
    
    
    
    
    private function email($report, $profile)
    {       
        $email = $profile["email"];
        //send email
        Mail::send('emails.report', ["email"=>$email, "report" => $report, "profile" => $profile], function($message) use ($email)
        {
            $message->to($email)->subject("Your report is ready");
        });     
          
    }     
    
    
    private function managementEmail($profile)
    {       
        $email = $profile["email"];
        //send email
        Mail::send('emails.management', ["email"=>$email, "profile" => $profile], function($message) use ($email)
        {
            $message->to($email)->subject("Resources for you");
        });     
          
    }   






    public function showadmin()
    {
        
        error_reporting(0);
        @ini_set('display_errors', 0);        
        
        $currentUser = JWTAuth::parseToken()->authenticate();
        
        
        //get average per construct broken grouped by role and intent
        
        $averages = array(
            "safety" => array(
                "overall" => 0,
                //intents added dynamically and broken down by role
            ),
            "sustainability" => array(
                "overall" => 0,
                //intents added dynamically and broken down by role
            ),
            "satisfaction" => array(
                "overall" => 0,
                //intents added dynamically and broken down by role
            )            
            
        );
        
        $averagesCount = array(
            "safety" => array(
                "overall" => 0,
                //intents added dynamically and broken down by role
            ),
            "sustainability" => array(
                "overall" => 0,
                //intents added dynamically and broken down by role
            ),
            "satisfaction" => array(
                "overall" => 0,
                //intents added dynamically and broken down by role
            )            
            
        );        
        
        
        $polarAverages = array(
            "volunteer" => array(
                "safety" => 0,
                "sustainability" => 0,
                "satisfaction" => 0  
            ),
            "leader" => array(
                "safety" => 0,
                "sustainability" => 0,
                "satisfaction" => 0 
            )
        );
        
        
        $polarAveragesCount = array(
            "volunteer" => array(
                "safety" => 0,
                "sustainability" => 0,
                "satisfaction" => 0  
            ),
            "leader" => array(
                "safety" => 0,
                "sustainability" => 0,
                "satisfaction" => 0 
            )
        );        
        
        $fieldGroupMap = array(
            1 => "sustainability",
            3 => "safety", 
            4 => "satisfaction"
        );
        
        $roleMap = array(
            1 => "leader",
            2=> "volunteer"
        );
        
        
        $textResults = array();
        
        $submissions = Profile::where("created_at", "<", "2020-11-01")->orderBy('created_at', 'desc')->get();
        
        foreach ($submissions as $key=>$submission){
            $answers = Answer::where("user_id", $submission->user_id)->get();
            $submissions[$key]["answers"] = $answers;
            foreach ($answers as $key2=>$answer){
                
    
                $field = Field::find($answer->field_id);


                if (($field->type === "Select") && $answer->answer && array_key_exists($field->field_group_id, $fieldGroupMap)  && $answer->skipped !== 1){
                    
                    $fieldGroup = $fieldGroupMap[$field->field_group_id];
                    $role = $roleMap[$submission->role_id];
                    
  
                    $averages[$fieldGroup]["overall"] = $averages[$fieldGroup]["overall"] + $answer->answer;
                    $averagesCount[$fieldGroup]["overall"] +=1;
                    
                    if (!empty($field->anchor)){
                        
                        if (array_key_exists($field->anchor, $averages[$fieldGroup])){
                            
                            $averages[$fieldGroup][$field->anchor]["overall"] = $averages[$fieldGroup][$field->anchor]["overall"] + $answer->answer;
                            
                            $averagesCount[$fieldGroup][$field->anchor]["overall"] +=1;
                            
                            if (array_key_exists($role, $averages[$fieldGroup][$field->anchor])){
                                
                                $averages[$fieldGroup][$field->anchor][$role] = $averages[$fieldGroup][$field->anchor][$role] + $answer->answer;
                                $averagesCount[$fieldGroup][$field->anchor][$role] +=1;
                                
                            }
                            else{
                                $averages[$fieldGroup][$field->anchor][$role] = ($answer->answer);
                                $averagesCount[$fieldGroup][$field->anchor][$role] = 1;
                            }

                        }
                        else{
                            $averages[$fieldGroup][$field->anchor] = array(
                                "overall" => $answer->answer
                            );
                            $averages[$fieldGroup][$field->anchor][$role] = ($answer->answer);
                            
                            $averagesCount[$fieldGroup][$field->anchor] = array(
                                "overall" => 1
                            );
                            $averagesCount[$fieldGroup][$field->anchor][$role] = 1;                            
                            
                            

                        }
                    }
                    
                    
                    $polarAverages[$role][$fieldGroup] = $polarAverages[$role][$fieldGroup] + $answer->answer;
                    $polarAveragesCount[$role][$fieldGroup] += 1;
                    
                    
                }
                else if ($field->type === "Text"){
                    if (array_key_exists($field->question, $textResults)){
                        //array_push($textResults[$field->question], $answer->answer);
                    }
                    else{
                        //$textResults[$field->question] = array($answer->answer);
                    }
                }
                
            }            
        }
        


        
        foreach ($averages as $fieldGroup => $average){
            
            foreach ($average as $anchor => $subAverage){
                
               
                
                if ($anchor === "overall"){
                    $averages[$fieldGroup][$anchor] = $averages[$fieldGroup][$anchor] / $averagesCount[$fieldGroup][$anchor];
                }
                else{
                    $averages[$fieldGroup][$anchor]["overall"] = round($averages[$fieldGroup][$anchor]["overall"] / $averagesCount[$fieldGroup][$anchor]["overall"],2);
                    foreach ($roleMap as $role){

                        $averages[$fieldGroup][$anchor][$role] = round($averages[$fieldGroup][$anchor][$role] / $averagesCount[$fieldGroup][$anchor][$role],2);

                    }

                }
                
            }
        }
        
        
        foreach ($polarAverages as $role => $polarAverage){
            
            foreach ($polarAverage as $fieldGroup => $subPolarAverage){
                $polarAverages[$role][$fieldGroup] = round($polarAverages[$role][$fieldGroup] / $polarAveragesCount[$role][$fieldGroup],2);
            }
            
            
        }
     
        
        $this->loadAdminPDF($averages, $polarAverages, $textResults);
        
    }
    
    private function loadAdminPDF($averages, $polarAverages, $textResults){

      
        
        
        $data = array(
            'averages'=>json_encode($averages),
            'polarAverages'=>json_encode($polarAverages),
            'textResults' => json_encode($textResults)
        );
        
        $request = \PhantomJs::post('https://volpoll.org.au/api/resources/views/pdfs/adminreport.php');
        $request->setRequestData($data);
        $request->setDelay(5);
        
        $response = \PhantomJs::isLazy()->send($request);



        if($response->getStatus() === 200 || $response->getStatus() === 301) {
            
            
            print_r(($response->getContent()));
            die();
            
            // Dump the requested page content
            $pdf = PDF::loadHTML($response->getContent());

            
            //$pdf->save('reports/VolPollPlusReport.pdf');  

            
            //$report->file = 'reports/VolPollPlusReport.pdf';
            //$report->save();
            
            $pdf->stream('VolPollPlusReport.pdf');  

        }
        else{
  
            throw new NotFoundHttpException;
        }        
        
        
        
        
        
    }    
    
    









    
    
    
    public function update(Request $request, $id)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();

        $report = Report::find($id);
        if(!$report)
            throw new NotFoundHttpException;
        

        $report->fill($request->all());

        if($report->save()){

            return $this->response->noContent();
        }
        else
            return $this->response->error('could_not_update_report', 500);        
    }

   
    

    public function destroy($id)
    {
        $currentUser = JWTAuth::parseToken()->authenticate();

        $report = Report::find($id);

        if(!$report)
            throw new NotFoundHttpException;

        
        if($report->delete())
            return $this->response->noContent();
        else
            return $this->response->error('could_not_delete_report', 500);        
    }   
    
    
    
}



