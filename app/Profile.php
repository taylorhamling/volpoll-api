<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = [
        'user_id', 'role_id', 'first_name', 'last_name','email','group_type', 'group','position', 'is_peak','peak_details', 'postcode'
    ];
}