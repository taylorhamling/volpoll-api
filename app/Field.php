<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    protected $fillable = [
        'role_id', 'field_group_id', 'anchor', 'question', 'description', 'type', 'options', 'field_order'
    ];
}
