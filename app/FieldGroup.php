<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FieldGroup extends Model
{
    protected $fillable = [
        'name', 'description','report_description','report_ideas', 'field_group_order'
    ];
}
