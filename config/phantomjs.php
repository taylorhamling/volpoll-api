<?php

return [

    // phantomjs binary path
    'binary_path' => base_path() . '/vendor/bin/phantomjs.exe',

    // phantomjs run options
    'options' => [],

    // phantomjs debug mode
    'debug' => false,

    // phantomjs cache mode
    'cache' => false
];
