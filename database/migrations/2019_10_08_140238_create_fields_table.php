<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fields', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('role_id')->index();
            $table->integer('field_group_id')->index();
            $table->string('anchor')->nullable();
            $table->longText('question')->nullable();
            $table->longText('description')->nullable();
            $table->string('type')->nullable();
            $table->longText('options')->nullable();
            $table->integer('field_order')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fields');
    }
}
